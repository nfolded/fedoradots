# .bashrc jul 2020

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

shopt -s histappend
export HISTCONTROL=ignoreboth:erasedups
HISTSIZE=1000
HISTFILESIZE=2000

export EDITOR=nvim

#path
export PATH="$PATH:$HOME/bin"

#go path
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"

# cool options for cool kids
shopt -s \
    autocd \
    globstar \
    checkwinsize \
    cdspell \
    dirspell \
    expand_aliases \
    dotglob \
    gnu_errfmt \
    histreedit \
    nocasematch

bind 'set completion-ignore-case on'
bind 'set show-all-if-ambiguous on'
bind 'set colored-stats on'
bind 'set completion-display-width 1'
bind 'TAB:menu-complete'

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob;
# Autocorrect typos in path names when using `cd`
shopt -s cdspell;

## Reset to normal: \033[0m
NORM="\033[0m"

## Colors:
RED="\033[0;31m"
CYN="\033[36m"

#prompt
export PS1="\n${CYN}[ \w ]\n${RED}▲  ${NORM}"

# aliases
alias ls="ls -a --color=auto"
alias ll="ls -lahF"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias rmd="rm -rfi"
alias c="clear"

# eitors
alias n="nvim"

#functions

# create a new directory and enter it
mkd() {
  mkdir -p "$1" && cd "$1"
}

# fzf search than neovim open
nf() {
  nvim $(fzf --height 40% --reverse)
}

# cd into previous dirs
declare -a DIRS

savedir() {
  local i
  for ((i=1;i<=9;i++)); do
    test "$1" = "${DIRS[$i]}" && return
  done
  for ((i=9;i>1;i--)); do
    DIRS[$i]="${DIRS[((i-1))]}"
  done
  DIRS[1]="$1"
}

showdirs() {
  local i=1
  while [ "${DIRS[$i]}" ]; do
    echo "$i: ${DIRS[$i]}"
    ((i++))
  done
}

gotodir() {
  local d
  showdirs
  printf "goto: "
  read -n 1 d
  echo
  cd "${DIRS[$d]}"
}

PROMPT_COMMAND=prompt_command
prompt_command() { savedir "$OLDPWD"; }

alias cdh=gotodir

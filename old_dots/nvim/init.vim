" neovim jul 2020

if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -flo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif

set nocompatible " be iMproved
call plug#begin('~/.local/share/nvim/plugged')

Plug 'fatih/vim-go'
Plug 'sheerun/vim-polyglot'

Plug 'jiangmiao/auto-pairs'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'junegunn/fzf'
Plug 'lambdalisue/fern.vim'

Plug 'sainnhe/forest-night'
Plug 'arcticicestudio/nord-vim'
Plug 'junegunn/goyo.vim'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'liuchengxu/vista.vim'

call plug#end()

" coc
" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

"use return to expand suggestions
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() :
  \"\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

"vista
let g:vista_default_executive = 'coc'

" don't back up
set nobackup
set noswapfile

" syntax on
syntax enable

filetype plugin indent on

" minimum lines above/below cursor"
set scrolloff=5

" don't show mode, it is in the statusline
set noshowmode

"set split open below current window
set splitbelow

" use wildmenu command line completion
set wildmenu
set wildmode=list:longest,full

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

" use the mouse
"set mouse=nv

" show line numbers
set relativenumber
set number

" set width number space
set nuw=5

" Enable hidden buffers
set hidden

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Be smart when using tabs ;)
set smarttab

" 1 tab == 2 spaces
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

set encoding=utf-8

" file viewer
let g:netrw_banner       = 0    "don't show the instructions
let g:netrw_keepdir      = 0
let g:netrw_liststyle    = 0    " 0 = thin 3 = tree
let g:netrw_browse_split = 0    " 0 open in current window, 4 for split

set ai "Auto indent
set si "Smart indent

"wrap lines
set wrap "Wrap lines

" backspace acts as it should
set backspace=eol,start,indent
set whichwrap+=<,>h,l

"turn on spell check for markdown files
autocmd BufRead,BufNewFile *.md setlocal textwidth=80 spell
autocmd BufRead,BufNewFile *.txt setlocal textwidth=80 spell

"" Dictionary
set dictionary=/usr/share/dict/words

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

"strip trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Remember info about open buffers on close
set viminfo^=%

" Show unwanted whitespace and linebreak
set list
set listchars=tab:\ \ ,extends:❯,precedes:❮,trail:.
set showbreak=¬

" Turn off sound
set vb
set t_vb=

" Better display for messages
set cmdheight=2
" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300
" don't give |ins-completion-menu| messages.
set shortmess+=c
" always show signcolumns
set signcolumn=yes

"key mappings
"""""""""""""""""""""""""""""""""""""

"let mapleader = "\<Space>"
map <Space> <Leader>

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

inoremap jk <ESC>
vnoremap jk <ESC>
tnoremap jk <C-\><C-n>
"tnoremap <Esc> <C-\><C-n>

" Easy saving
nnoremap <leader>w :w<CR>

"don't mess with copy and paste indenting
set pastetoggle=<leader>P

" use the system clipboard
set clipboard=unnamedplus

nnoremap <leader>bb :buffers<CR>:buffer<Space>

"go to the next open buffer
nnoremap <leader>bn :bnext<CR>

"go to the previous open buffer
"nnoremap <leader>bv :bprevious<CR>

"ripgrep
nnoremap <leader>r :Rg<Space>

"fuzzy with fzf
let g:fzf_layout = { 'window': '16new' }
nnoremap <leader>f :FZF<CR>

"fern explorer
noremap <leader>n :Fern . -drawer -toggle<CR>

"cd into direcotry of currently open file
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>

"resizing a vertical window split
map <silent> <C-h> <C-w><
map <silent> <C-l> <C-w>>

"move around more easily in terminal splits
tnoremap <C-a>h <C-\><C-n><C-w>h
tnoremap <C-a>j <C-\><C-n><C-w>j
tnoremap <C-a>k <C-\><C-n><C-w>k
tnoremap <C-a>l <C-\><C-n><C-w>l

" map moving between splits to C-a because that is what I use in tmux
nnoremap <C-a>h <C-w>h
nnoremap <C-a>j <C-w>j
nnoremap <C-a>k <C-w>k
nnoremap <C-a>l <C-w>l

"quit search
nnoremap <leader><space> :nohlsearch<CR>

" Treat long lines as break lines (useful when moving around in them)
noremap j gj
noremap k gk

nnoremap H 0
nnoremap L A
vnoremap H 0
vnoremap L $

" Helper Functions and Mappings {{{
" Easily manage quick fix windows
map <silent> <C-n> :cnext<CR>
map <silent> <C-m> :cprevious<CR>
nnoremap <silent> <leader>q :cclose<CR>

"locationlist
nnoremap <silent> <leader>l :lclose<CR>
nnoremap <silent> <leader>lo :lopen<CR>

" Open terminal in split below and resize it
nnoremap <silent><leader>t :sp<bar>term<CR><c-w>J:resize15<CR>

" Go
let g:go_fmt_command = "goimports"
let g:go_def_mode = "gopls"
let g:go_info_mode = "gopls"
let g:go_metalinter_enabled = ["vet", "golint"]

augroup go
  autocmd!
  autocmd FileType go nmap <silent> <Leader>gi <Plug>(go-info)
  autocmd FileType go nmap <silent> <leader>gr  <Plug>(go-run)
  autocmd FileType go nmap <silent> <leader>gt  <Plug>(go-test)
  autocmd FileType go nmap <silent> <Leader>gd <Plug>(go-def)
augroup END

let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1

"colors & appearance
"""""""""""""""""""
 if has('nvim') || has('termguicolors')
   set termguicolors
 endif

set background=dark

" let g:nord_cursor_line_number_background = 1
" let g:nord_italic = 1
" let g:nord_italic_comments = 1
" colorscheme nord

let g:forest_night_enable_italic = 1
colorscheme forest-night

hi Comment        cterm=italic  gui=italic
hi SignColumn     ctermbg=NONE  ctermfg=1 guibg=NONE
hi Pmenu          ctermbg=11    ctermfg=13
hi PmenuSel       ctermbg=13    ctermfg=11

"statusline
source $HOME/.config/nvim/statusline.vim
